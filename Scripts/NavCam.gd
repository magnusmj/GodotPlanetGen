extends Position3D

var velocity: Vector3
var angular_vecocity: Vector3
var speed = 1.0

var dbg = false
var mouse_captured = true

signal my_signal(value)

func _ready():	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	

func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		mouse_captured = not mouse_captured
		if mouse_captured:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if event is InputEventMouseMotion:
		var x_delta = event.relative.x * 0.001
		var y_delta = event.relative.y * 0.001
		#$Camera.rotate_x(deg2rad(-event.relative.x * O.config.mouse_sensitivity))
		
		
		
		var basis = $Camera.transform.basis.orthonormalized()
		$Camera.transform = $Camera.transform.rotated(basis.y, -x_delta) # rotate_object_local(Vector3(x_delta, 0, y_delta).normalized(), 0.01)#_y(deg2rad(x_delta))
		$Camera.transform = $Camera.transform.rotated(basis.x, -y_delta)
	if event is InputEventMouseButton and event.pressed:
		if event.button_index == BUTTON_WHEEL_UP :
			speed *= 1.25
		if event.button_index == BUTTON_WHEEL_DOWN:
			speed *= 0.8

func _process(delta):
	if Input.is_action_just_pressed("BREAK"):
		get_tree().quit()
	var direction = Vector3()
	var head_basis = $Camera.get_global_transform().basis
	
	if Input.is_action_just_pressed("ui_focus_next"):
		if not dbg:
			VisualServer.set_debug_generate_wireframes(true)
			var vp = get_viewport()
			vp.debug_draw = 3
		else:
			VisualServer.set_debug_generate_wireframes(false)
			var vp = get_viewport()
			vp.debug_draw = 0
		dbg = not dbg

	if Input.is_action_pressed("move_forward"):
		direction -= head_basis.z
	elif Input.is_action_pressed("move_backward"):
		direction += head_basis.z
	
	if Input.is_action_pressed("move_left"):
		direction -= head_basis.x
	elif Input.is_action_pressed("move_right"):
		direction += head_basis.x
		
	if Input.is_action_pressed("pivot_left"):
		var basis = $Camera.transform.basis
		$Camera.transform = $Camera.transform.rotated(basis.z, delta)
	if Input.is_action_pressed("pivot_right"):
		var basis = $Camera.transform.basis
		$Camera.transform = $Camera.transform.rotated(basis.z, -delta)
	
	velocity += direction*speed*delta
	transform.origin += velocity*delta
	if not Input.is_action_pressed("run"):
		velocity *= (1-delta*5)

