using System;
using Godot;

namespace SegmentedPlanet
{
	public class Segment : Spatial
	{
		private ArrayMesh arrayMesh;
		private Planet planet;		
		private QuadTree quadTree;
				
		private Vertex vtx1;
		private Vertex vtx2;
		private Vertex vtx3;
		private Vertex vtx4;
		
		public int treeMaxDepth;		

		public delegate void SurfaceRemovedHandler(object sender, SurfaceRemovedEventArgs e);
		public event SurfaceRemovedHandler SurfaceRemoved;
		public event EventHandler PositionMoved;
		
		

		public Planet Planet => planet;
		public Vector3 LastCamPosition { get; set; }

		public void RemoveSurface(QuadTree surf)
		{
			if (surf == null)
			{
				GD.Print("surf not and instance");
				return;
			}

			if (surf.SurfaceIndex == -1)
			{
				//GD.Print("Can't remove surface with index -1");
				return;
			}
			arrayMesh.SurfaceRemove(surf.SurfaceIndex);		
			SurfaceRemoved?.Invoke(this, new SurfaceRemovedEventArgs {index = surf.SurfaceIndex});
		}

		public void AddSurface(QuadTree surf)
		{
			surf.SurfaceIndex = arrayMesh.GetSurfaceCount();
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, surf.MeshArray, compressFlags: 0);
		}

		public void Init(Vertex vtx1, Vertex vtx2, Vertex vtx3, Vertex vtx4)
		{			
			arrayMesh = (ArrayMesh)GetNode<MeshInstance>("MeshInstance").Mesh;
			var transform = Transform;        
			var center = (vtx1.pos + vtx2.pos + vtx3.pos + vtx4.pos)/4;
			this.vtx1 = vtx1;
			this.vtx1.pos -= center;
			this.vtx2 = vtx2;
			this.vtx2.pos -= center;
			this.vtx3 = vtx3;
			this.vtx3.pos -= center;
			this.vtx4 = vtx4;
			this.vtx4.pos -= center;
			transform.origin = center;
			Transform = transform;	
		}
	}        
	
}
