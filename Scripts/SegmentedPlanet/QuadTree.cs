using System;
using System.Collections.Generic;
using System.Linq;
using Godot;

namespace SegmentedPlanet
{
   public class QuadTree
	{
		private static Stack<QuadTree> pool = new Stack<QuadTree>();
		private Godot.Collections.Array meshArray = new Godot.Collections.Array();
		private Segment segment;
		private QuadTree parent = null;
		private Face[] faces;
		private Vertex vtx1; // = new Vertex();
		private Vertex vtx2; // = new Vertex();
		private Vertex vtx3; // = new Vertex();
		private Vertex vtx4; // = new Vertex();
		private Vector3 center;
		private float width;
		private int surfaceIndex = -1;
		private QuadTree[] subDivisions = new QuadTree[16];
		private bool disabled = false;

		public QuadSide side = QuadSide.Left;


		private Vector3[] verts; // = new Vector3[faces.Length * 3];
		private Vector3[] norms; // = new Vector3[faces.Length * 3];
		private float[] tangents; // = new float[faces.Length * 3 * 4];
		private Vector2[] uvs; // = new Vector2[faces.Length * 3];

		public QuadTree(Segment segment, QuadSide side, Vector3 vct1, Vector3 vct2, Vector3 vct3, Vector3 vct4)
		{
			faces = new Face[32];
			verts = new Vector3[faces.Length * 3];
			norms = new Vector3[faces.Length * 3];
			tangents = new float[faces.Length * 3 * 4];
			uvs = new Vector2[faces.Length * 3];

			meshArray.Resize((int) Mesh.ArrayType.Max);

			vtx1.pos = vct1;
			vtx1.norm = vct1.Normalized();
			vtx1.uv = new Vector2(0, 0);
			vtx2.pos = vct2;
			vtx2.norm = vct2.Normalized();
			vtx2.uv = new Vector2(1, 0);
			vtx3.pos = vct3;
			vtx3.norm = vct3.Normalized();
			vtx3.uv = new Vector2(0, 1);
			vtx4.pos = vct4;
			vtx4.norm = vct4.Normalized();
			vtx4.uv = new Vector2(1, 1);

			this.side = side;
			Init(segment, null, vtx1, vtx2, vtx3, vtx4);
		}

		public QuadTree(Segment segment, QuadTree parent, Vertex v1, Vertex v2, Vertex v3, Vertex v4)
		{
			faces = new Face[32];
			verts = new Vector3[faces.Length * 3];
			norms = new Vector3[faces.Length * 3];
			tangents = new float[faces.Length * 3 * 4];
			uvs = new Vector2[faces.Length * 3];
			meshArray.Resize((int) Mesh.ArrayType.Max);
			Init(segment, parent, v1, v2, v3, v4);

		}

		public void Init(Segment segment, QuadTree parent, Vertex v1, Vertex v2, Vertex v3, Vertex v4)
		{
			this.segment = segment;
			this.parent = parent;
			if (parent != null) side = parent.side;
			vtx1 = v1;
			vtx2 = v2;
			vtx3 = v3;
			vtx4 = v4;
			center = (v1.pos + v2.pos + v3.pos + v4.pos) / 4;
			width = (v1.pos - v4.pos).Length();

			MakeQuads();
			CreateMesh(faces);
		}



		public int Depth
		{
			get
			{
				if (parent == null) return 0;
				else return parent.Depth + 1;
			}
		}

		private void PlanetOnPositionMoved(object sender, EventArgs e)
		{
			if (disabled) return;
			float dist = (segment.LastCamPosition - center).Length();
			dist = Mathf.Min(dist, (segment.LastCamPosition - vtx1.pos).Length());
			dist = Mathf.Min(dist, (segment.LastCamPosition - vtx2.pos).Length());
			dist = Mathf.Min(dist, (segment.LastCamPosition - vtx3.pos).Length());
			dist = Mathf.Min(dist, (segment.LastCamPosition - vtx4.pos).Length());

			float angleDiff = center.AngleTo(segment.LastCamPosition);

			if (surfaceIndex != -1 && dist < 2 * width) SubDivide();
			else if (surfaceIndex == -1 && dist > 2 * width) UnSubDivide();

		}


		private void PlanetOnSurfaceRemoved(object sender, SurfaceRemovedEventArgs args)
		{
			if (args.index == surfaceIndex) surfaceIndex = -1;
			else if (args.index < surfaceIndex) surfaceIndex--;
		}

		public bool Disabled
		{
			get => disabled;
			set
			{
				disabled = value;
				if (disabled)
				{
					segment.PositionMoved -= PlanetOnPositionMoved;
					segment.SurfaceRemoved -= PlanetOnSurfaceRemoved;
					foreach (var subdiv in subDivisions.Reverse())
					{
						if (subdiv != null && !subdiv.Disabled)
						{
							subdiv.Disabled = true;
							if (subdiv.surfaceIndex != -1)
								segment.RemoveSurface(subdiv);
						}
					}
				}
				else
				{
					segment.PositionMoved += PlanetOnPositionMoved;
					segment.SurfaceRemoved += PlanetOnSurfaceRemoved;
				}
			}
		}

		public Godot.Collections.Array MeshArray
		{
			get { return meshArray; }
		}

		public int SurfaceIndex
		{
			get => surfaceIndex;
			set => surfaceIndex = value;
		}

		public Vertex CalcMedian(Vertex v1, Vertex v2)
		{
			Vertex median;
			median.pos = (v1.pos + v2.pos).Normalized() * Planet.Radius;
			median.norm = median.pos.Normalized();
			median.uv = (v1.uv + v2.uv) / 2;
			return median;
		}

		public void MakeQuads()
		{
			Vertex vtx12 = CalcMedian(vtx1, vtx2);
			Vertex vtx34 = CalcMedian(vtx3, vtx4);
			Vertex vtx13 = CalcMedian(vtx1, vtx3);
			Vertex vtx24 = CalcMedian(vtx2, vtx4);
			Vertex vtxc = CalcMedian(vtx1, vtx4);

			MakeQuads(0, vtx1, vtx12, vtx13, vtxc);
			MakeQuads(8, vtx12, vtx2, vtxc, vtx24);
			MakeQuads(16, vtx13, vtxc, vtx3, vtx34);
			MakeQuads(24, vtxc, vtx24, vtx34, vtx4);
		}

		public void MakeQuads(int index, Vertex v1, Vertex v2, Vertex v3, Vertex v4)
		{
			Vertex v12 = CalcMedian(v1, v2);
			Vertex v34 = CalcMedian(v3, v4);
			Vertex v13 = CalcMedian(v1, v3);
			Vertex v24 = CalcMedian(v2, v4);
			Vertex vc;
			if (index == 0 || index == 24)
				vc = CalcMedian(v1, v4);
			else
				vc = CalcMedian(v2, v3);

			faces[index + 0] = MakeFace(v1, v12, vc);
			faces[index + 1] = MakeFace(vc, v13, v1);
			faces[index + 2] = MakeFace(vc, v12, v2);
			faces[index + 3] = MakeFace(v2, v24, vc);

			faces[index + 4] = MakeFace(v3, v13, vc);
			faces[index + 5] = MakeFace(vc, v34, v3);
			faces[index + 6] = MakeFace(vc, v24, v4);
			faces[index + 7] = MakeFace(v4, v34, vc);

		}

		public Face MakeFace(Vertex v1, Vertex v2, Vertex v3)
		{
			Face face;
			face.v1 = v1;
			face.v2 = v2;
			face.v3 = v3;
			return face;
		}

		public void CreateMesh(Face[] faces)
		{
			int i = 0;
			foreach (Face face in faces)
			{
				verts[i] = face.v1.pos;
				norms[i] = face.v1.norm;
				uvs[i] = face.v1.uv;
				Vector3 tangent; // = face.v1.pos.Cross(Vector3.Up);
				if (side == QuadSide.Up || side == QuadSide.Down)
					tangent = verts[i].Cross(Vector3.Forward).Normalized();
				else
					tangent = verts[i].Cross(Vector3.Up).Normalized();
				tangent = verts[i].Cross(tangent).Normalized();
				tangents[4 * i] = tangent.x;
				tangents[4 * i + 1] = tangent.y;
				tangents[4 * i + 2] = tangent.z;
				tangents[4 * i + 3] = 1;
				i++;
				verts[i] = face.v2.pos;
				norms[i] = face.v2.norm;
				uvs[i] = face.v2.uv;
				if (side == QuadSide.Up || side == QuadSide.Down)
					tangent = verts[i].Cross(Vector3.Forward).Normalized();
				else
					tangent = verts[i].Cross(Vector3.Up).Normalized();
				tangent = verts[i].Cross(tangent).Normalized();
				tangents[4 * i] = tangent.x;
				tangents[4 * i + 1] = tangent.y;
				tangents[4 * i + 2] = tangent.z;
				tangents[4 * i + 3] = 1;
				i++;
				verts[i] = face.v3.pos;
				norms[i] = face.v3.norm;
				uvs[i] = face.v3.uv;
				if (side == QuadSide.Up || side == QuadSide.Down)
					tangent = verts[i].Cross(Vector3.Forward).Normalized();
				else
					tangent = verts[i].Cross(Vector3.Up).Normalized();
				tangent = verts[i].Cross(tangent).Normalized();
				tangents[4 * i] = tangent.x;
				tangents[4 * i + 1] = tangent.y;
				tangents[4 * i + 2] = tangent.z;
				tangents[4 * i + 3] = 1;
				i++;
			}

			meshArray[(int) Mesh.ArrayType.Vertex] = verts;
			meshArray[(int) Mesh.ArrayType.Normal] = norms;
			meshArray[(int) Mesh.ArrayType.TexUv] = uvs;
			meshArray[(int) Mesh.ArrayType.Tangent] = tangents;
		}

		public QuadTree GetQuadTree(Segment segment, QuadTree parent, Vertex v1, Vertex v2, Vertex v3, Vertex v4)
		{
			QuadTree quadTree;
			if (pool.Count > 0)
			{
				quadTree = pool.Pop();
				quadTree.Init(segment, parent, v1, v2, v3, v4);
			}
			else
			{
				quadTree = new QuadTree(this.segment, this, v1, v2, v3, v4);
			}

			return quadTree;
		}

		public void SubDivide()
		{
			//GD.Print("SubDivide");
			if (Depth == segment.treeMaxDepth) return;
			segment.RemoveSurface(this);
			if (subDivisions[0] == null)
			{
				for (int i = 0; i < 32; i += 4)
				{
					var v1 = faces[i + 0].v1;
					var v2 = faces[i + 0].v2;
					var v3 = faces[i + 1].v2;
					var v4 = faces[i + 1].v1;
					subDivisions[i / 2] = GetQuadTree(this.segment, this, v1, v2, v3, v4);

					v1 = faces[i + 2].v2;
					v2 = faces[i + 2].v3;
					v3 = faces[i + 3].v3;
					v4 = faces[i + 3].v2;
					subDivisions[i / 2 + 1] = GetQuadTree(this.segment, this, v1, v2, v3, v4);
				}
			}

			foreach (var subdiv in subDivisions)
			{
				if (subdiv != null)
				{
					subdiv.Disabled = false;

					segment.AddSurface(subdiv);
				}
			}
		}

		public void UnSubDivide()
		{
			//GD.Print("UnSubDivide");
			if (parent == null || disabled == false)
			{
				if (subDivisions[0] != null)
				{
					for (int i = 0; i < subDivisions.Length; i++)
					{
						var subdiv = subDivisions[i];

						subdiv.Disabled = true;
						segment.RemoveSurface(subdiv);
						pool.Push(subDivisions[i]);
						subDivisions[i] = null;
					}

				}

				segment.AddSurface(this);
			}
		}

		public void Detach()
		{
			segment.PositionMoved -= PlanetOnPositionMoved;
			segment.SurfaceRemoved -= PlanetOnSurfaceRemoved;

			if (subDivisions[0] != null)
			{
				foreach (var subdiv in subDivisions)
				{
					if (subdiv != null)
						subdiv.Detach();
				}
			}
		}
	}
}