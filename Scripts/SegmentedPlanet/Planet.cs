using System;
using Godot;

namespace SegmentedPlanet
{	
	[Tool]
	public class Planet : Spatial
	{
		[Export] private bool create_mesh = false;
		[Export] private NodePath WorldEnvironmentPath;
		[Export] private Gradient atmosphereColor;
		[Export] private Color horizonColor;
		[Export] private float atmosphereHeight = 100;
		[Export] private float terrainHeight = 50;
		[Export] private float radius = 6000;
		[Export] private PackedScene segmentPrefab;
		
		private Spatial camPosition;
		private Spatial segments;
		private WorldEnvironment worldEnvironment;
		private DirectionalLight sun;        
		private Vector3 lastCamPosition;
		private const int faceCount = 32;
		private Face[] faces = new Face[faceCount];
		
		public delegate void SurfaceRemovedHandler(object sender, SurfaceRemovedEventArgs e);
		public event SurfaceRemovedHandler SurfaceRemoved;
		public event EventHandler PositionMoved;

		public override void _Ready()
		{
			base._Ready();
			worldEnvironment = GetNode<WorldEnvironment>(WorldEnvironmentPath);
			segments = GetNode<Spatial>("Segments");
			camPosition = GetNode<Spatial>("Position3D");
			
		}

		public override void _Process(float delta)
		{
			if (create_mesh)
			{
				create_mesh = false;
				InitSegments();				
			}
	
			var campPosition = camPosition.Transform.origin;
			campPosition -= campPosition.Normalized() * terrainHeight/2;
			if ((lastCamPosition - campPosition).Length() > 0.00001 * radius)
			{						
				lastCamPosition = campPosition;
				
				PositionMoved?.Invoke(this, EventArgs.Empty);
				//GD.Print("Position moved");
				if (worldEnvironment != null)
				{
					Vector3 normal = lastCamPosition.Normalized();				
					Vector3 tangent = normal.Cross(Vector3.Up).Normalized();
					Vector3 biNormal = normal.Cross(tangent).Normalized();
					Basis skyBasis = new Basis(biNormal, normal, tangent);
					worldEnvironment.Environment.BackgroundSkyOrientation = skyBasis;
					if (sun != null)
					{
						ProceduralSky sky = (ProceduralSky)worldEnvironment.Environment.BackgroundSky;
						float lat = Mathf.Deg2Rad(sky.SunLatitude);
						float lon = Mathf.Deg2Rad(sky.SunLongitude);
						var sunTransform = sun.Transform;
						sunTransform.basis = skyBasis;
						sunTransform.basis = sunTransform.basis.Rotated(sunTransform.basis.y, Mathf.Pi-lon);
						sunTransform.basis = sunTransform.basis.Rotated(sunTransform.basis.x, -lat);											
						
						sun.Transform = sunTransform;
						float surfaceDistance = lastCamPosition.Length() - radius;
						float surfaceDistanceRatio = Mathf.Clamp(surfaceDistance / atmosphereHeight, 0, 1);
						sky.SkyTopColor = atmosphereColor.Interpolate(surfaceDistanceRatio);
						sky.SkyHorizonColor = horizonColor.LinearInterpolate(sky.SkyTopColor, surfaceDistanceRatio);
						sky.GroundHorizonColor = sky.SkyHorizonColor;
						sky.GroundBottomColor = atmosphereColor.Interpolate(0);
						sky.GroundCurve = Mathf.Max(0.01f, Mathf.Pow(2, surfaceDistance/5000)-0.5f);
						sky.SkyCurve = sky.GroundCurve;
						//worldEnvironment.Environment.AmbientLightSkyContribution = 1-surfaceDistanceRatio;
						Color fogColor =  sky.SkyHorizonColor;
						fogColor.a = 1.0f - surfaceDistanceRatio;
						worldEnvironment.Environment.FogColor = fogColor;
						
					}
				}			
			}
		}

		public Vector3 LastCamPosition => lastCamPosition;
		public static float Radius { get; set; }
		
		private void InitSegments()
		{	        
			foreach (var child in GetNode<Spatial>("Segments").GetChildren())
			{
				if (child != null)
				{
					((Node)child).QueueFree();
				}
			}
			Vector3[] cubeVerts = {
				new Vector3(-1, -1, -1).Normalized() * radius,
				new Vector3(-1, -1, 1).Normalized() * radius,
				new Vector3(-1, 1, -1).Normalized() * radius,
				new Vector3(-1, 1, 1).Normalized() * radius,
				new Vector3(1, -1, -1).Normalized() * radius,
				new Vector3(1, -1, 1).Normalized() * radius,
				new Vector3(1, 1, -1).Normalized() * radius,
				new Vector3(1, 1, 1).Normalized() * radius,
			};
			makeSegments(QuadSide.Left,  cubeVerts[3], cubeVerts[1], cubeVerts[2], cubeVerts[0]);
			
		}


		private void makeSegments(QuadSide side, Vector3 vct1, Vector3 vct2, Vector3 vct3, Vector3 vct4)
		{
			Vertex vtx1;
			Vertex vtx2;
			Vertex vtx3;
			Vertex vtx4;
			vtx1.pos = vct1;
			vtx1.norm = vct1.Normalized();
			vtx1.uv = new Vector2(0, 0);
			vtx2.pos = vct2;
			vtx2.norm = vct2.Normalized();
			vtx2.uv = new Vector2(1, 0);
			vtx3.pos = vct3;
			vtx3.norm = vct3.Normalized();
			vtx3.uv = new Vector2(0, 1);
			vtx4.pos = vct4;
			vtx4.norm = vct4.Normalized();
			vtx4.uv = new Vector2(1, 1);
			MakeQuads(0, 1, faces, vtx1, vtx2, vtx3, vtx4);
			
			for (int i = 0; i < faceCount; i += 4)
			{
				var v1 = faces[i + 0].v1;
				var v2 = faces[i + 0].v2;
				var v3 = faces[i + 1].v2;
				var v4 = faces[i + 1].v1;
				//subDivisions[i / 2] = GetQuadTree(this.planet, this, v1, v2, v3, v4);
				makeSegment(v1, v2, v3, v4);

				v1 = faces[i + 2].v2;
				v2 = faces[i + 2].v3;
				v3 = faces[i + 3].v3;
				v4 = faces[i + 3].v2;
				makeSegment(v1, v2, v3, v4);
				//subDivisions[i / 2 + 1] = GetQuadTree(this.planet, this, v1, v2, v3, v4);
			}
		}

		public void makeSegment(Vertex vtx1, Vertex vtx2, Vertex vtx3, Vertex vtx4)
		{

			var segment = new Segment();	
			//var segment = segmentPrefab.Instance();// as Segment;
			if (segment != null)
			{
				
				GD.Print(segment.Name);				
				segments.AddChild(segment);
				//segment.Owner = segments;
				segment.Init(vtx1, vtx2, vtx3, vtx4);
				
			}
			else
			{
				GD.Print("segment is null");
			}

			GD.Print(segments.GetChildCount());
		}

		public Vertex CalcMedian(Vertex v1, Vertex v2)
		{
			Vertex median;
						
			median.pos = (v1.pos + v2.pos).Normalized() * Radius;
			median.norm = median.pos.Normalized();
			median.uv = (v1.uv + v2.uv) / 2;
			return median;
		}

		public Face MakeFace(Vertex v1, Vertex v2, Vertex v3)
		{
			Face face;
			face.v1 = v1;
			face.v2 = v2;
			face.v3 = v3;
			return face;
		}

		public void MakeQuads(int index, int divisions, Face[] faces, Vertex v1, Vertex v2, Vertex v3, Vertex v4)
		{
			Vertex v12 = CalcMedian(v1, v2);
			Vertex v34 = CalcMedian(v3, v4);
			Vertex v13 = CalcMedian(v1, v3);
			Vertex v24 = CalcMedian(v2, v4);
			Vertex vc = CalcMedian(CalcMedian(v1, v4), CalcMedian(v2, v3));
			int indexStep = divisions * 8;
			if (divisions>0)
			{
				MakeQuads(index, divisions-1, faces, v1, v12, v13, vc);
				MakeQuads(index + indexStep, divisions-1, faces, v12, v2, vc, v24);
				MakeQuads(index + 2*indexStep, divisions-1, faces, v13, vc, v3, v34);
				MakeQuads(index + 3*indexStep, divisions-1, faces, vc, v24, v34, v4);
			}
			else
			{
				faces[index + 0] = MakeFace(v1, v12, vc);
				faces[index + 1] = MakeFace(vc, v13, v1);
				faces[index + 2] = MakeFace(vc, v12, v2);
				faces[index + 3] = MakeFace(v2, v24, vc);

				faces[index + 4] = MakeFace(v3, v13, vc);
				faces[index + 5] = MakeFace(vc, v34, v3);
				faces[index + 6] = MakeFace(vc, v24, v4);
				faces[index + 7] = MakeFace(v4, v34, vc);	
			}						
		}
	}
}
