using Godot;
using System;
using System.Collections.Generic;


public class Misc
{

}

public struct Vertex
{
	public Vector3 pos;
	public Vector3 norm;
	public Vector2 uv;
}
public struct Face
{
	public Vertex v1;
	public Vertex v2;
	public Vertex v3;
}

public enum QuadSide
{
	Up,
	Down,
	Left,
	Right,
	Front,
	Back
}

public class SurfaceRemovedEventArgs : EventArgs
{
	public int index { get; set; }
}
