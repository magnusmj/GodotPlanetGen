using Godot;
using System;
using System.Collections.Generic;

namespace SimplePlanet
{
	[Tool]
	public class Planet : Spatial
	{
		[Export] private bool create_mesh = false;
		[Export] private bool create_sub_mesh = false;
		[Export] private float radius = 2.0f;
		[Export] private int seed = 2;
		[Export] public int treeMaxDepth = 5;
		[Export] private NodePath WorldEnvironmentPath;
		[Export] private NodePath SunPath;
	
		[Export] private Gradient atmosphereColor;
		[Export] private Color horizonColor;
		[Export] private float atmosphereHeight = 100;
		[Export] private float terrainHeight = 50;
	
		ArrayMesh arrayMesh;
		QuadTree quadTreeUp;
		QuadTree quadTreeLeft;
		QuadTree quadTreeRight;
		QuadTree quadTreeFront;
		QuadTree quadTreeBack;
		QuadTree quadTreeDown;
	
		private WorldEnvironment worldEnvironment;
		private DirectionalLight sun;
		private Spatial camPosition;
		private Vector3 lastCamPosition;
		
	
		public delegate void SurfaceRemovedHandler(object sender, SurfaceRemovedEventArgs e);
		public event SurfaceRemovedHandler SurfaceRemoved;
		public event EventHandler PositionMoved;
	
		public override void _Ready()
		{
			worldEnvironment = GetNode<WorldEnvironment>(WorldEnvironmentPath);
			sun = GetNode<DirectionalLight>(SunPath);
			camPosition = GetNode<Spatial>("Position3D");
			InitMesh();
			
		}
	
		public Vector3 LastCamPosition => lastCamPosition;
	
		public override void _Process(float delta)
		{
			if (create_mesh)
			{
				InitMesh();
				create_mesh = false;
			}
	
			if (create_sub_mesh)
			{
	
			}
			var campPosition = camPosition.Transform.origin;
			campPosition -= campPosition.Normalized() * terrainHeight;
			if ((lastCamPosition - campPosition).Length() > 0.00001 * radius)
			{						
				lastCamPosition = campPosition;
				
				PositionMoved?.Invoke(this, EventArgs.Empty);
				//GD.Print("Position moved");
				if (worldEnvironment != null)
				{
					Vector3 normal = lastCamPosition.Normalized();				
					Vector3 tangent = normal.Cross(Vector3.Up).Normalized();
					Vector3 biNormal = normal.Cross(tangent).Normalized();
					Basis skyBasis = new Basis(biNormal, normal, tangent);
					worldEnvironment.Environment.BackgroundSkyOrientation = skyBasis;
					ProceduralSky sky = (ProceduralSky)worldEnvironment.Environment.BackgroundSky;
					float surfaceDistance = lastCamPosition.Length() - radius;
					float surfaceDistanceRatio = Mathf.Clamp(surfaceDistance / atmosphereHeight, 0, 1);
					if (sun != null)
					{
						
						float lat = Mathf.Deg2Rad(sky.SunLatitude);
						float lon = Mathf.Deg2Rad(sky.SunLongitude);
						var sunTransform = sun.Transform;
						sunTransform.basis = skyBasis;
						sunTransform.basis = sunTransform.basis.Rotated(sunTransform.basis.y, Mathf.Pi-lon);
						sunTransform.basis = sunTransform.basis.Rotated(sunTransform.basis.x, -lat);											
						
						sun.Transform = sunTransform;
						
						
						sky.SkyTopColor = atmosphereColor.Interpolate(surfaceDistanceRatio);
						sky.SkyHorizonColor = horizonColor.LinearInterpolate(sky.SkyTopColor, surfaceDistanceRatio);
						sky.GroundHorizonColor = sky.SkyHorizonColor;
						sky.GroundBottomColor = atmosphereColor.Interpolate(0);
						sky.GroundCurve = Mathf.Max(0.01f, Mathf.Pow(2, surfaceDistance/5000)-0.5f);
						sky.SkyCurve = sky.GroundCurve;
						
					}
					
					//worldEnvironment.Environment.AmbientLightSkyContribution = 1-surfaceDistanceRatio;
					Color fogColor =  horizonColor;
					fogColor.a = 1.0f - surfaceDistanceRatio;
					worldEnvironment.Environment.FogColor = fogColor;
					worldEnvironment.Environment.BackgroundColor = new Color(0, 0, 0.25f - surfaceDistanceRatio/4);
					worldEnvironment.Environment.AmbientLightColor = new Color(0, 0, 0.25f - surfaceDistanceRatio/4);

				}			
			}
		}
	
		public void RemoveSurface(QuadTree surf)
		{
			if (surf == null)
			{
				GD.Print("surf not and instance");
				return;
			}
	
			if (surf.SurfaceIndex == -1)
			{
				//GD.Print("Can't remove surface with index -1");
				return;
			}
			arrayMesh.SurfaceRemove(surf.SurfaceIndex);
			var eventArgs = new SurfaceRemovedEventArgs();
			eventArgs.index = surf.SurfaceIndex;
			SurfaceRemoved?.Invoke(this, eventArgs);
		}
	
		public void AddSurface(QuadTree surf)
		{
			surf.SurfaceIndex = arrayMesh.GetSurfaceCount();
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, surf.MeshArray, compressFlags: 0);
		}
	
		public float Radius => radius;
	
		public void InitMesh()
		{
			if (quadTreeUp != null)
			{
				quadTreeUp.Detach();
				quadTreeLeft.Detach();
				quadTreeRight.Detach();
				quadTreeFront.Detach();
				quadTreeBack.Detach();
				quadTreeDown.Detach();
			}
			arrayMesh = (ArrayMesh)GetNode<MeshInstance>("MeshInstance").Mesh ;
			for (int i = arrayMesh.GetSurfaceCount() - 1; i >= 0; i--)
				arrayMesh.SurfaceRemove(0);
	
			Vector3[] cubeVerts = {
				new Vector3(-1, -1, -1).Normalized() * radius,
				new Vector3(-1, -1, 1).Normalized() * radius,
				new Vector3(-1, 1, -1).Normalized() * radius,
				new Vector3(-1, 1, 1).Normalized() * radius,
				new Vector3(1, -1, -1).Normalized() * radius,
				new Vector3(1, -1, 1).Normalized() * radius,
				new Vector3(1, 1, -1).Normalized() * radius,
				new Vector3(1, 1, 1).Normalized() * radius,
			};
		
			quadTreeLeft = new QuadTree(this, QuadSide.Left, cubeVerts[3], cubeVerts[1], cubeVerts[2], cubeVerts[0]);
			quadTreeLeft.SurfaceIndex = 1;
			quadTreeLeft.Disabled = false;
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeLeft.MeshArray, compressFlags: 0);
		
			quadTreeRight = new QuadTree(this, QuadSide.Right, cubeVerts[6], cubeVerts[4], cubeVerts[7], cubeVerts[5]);
			quadTreeRight.SurfaceIndex = 2;
			quadTreeRight.Disabled = false;		
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeRight.MeshArray, compressFlags: 0);
	
			quadTreeFront = new QuadTree(this, QuadSide.Front, cubeVerts[2], cubeVerts[0], cubeVerts[6], cubeVerts[4]);
			quadTreeFront.SurfaceIndex = 3;
			quadTreeFront.Disabled = false;		
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeFront.MeshArray, compressFlags: 0);
	
			quadTreeBack = new QuadTree(this, QuadSide.Back, cubeVerts[7], cubeVerts[5], cubeVerts[3], cubeVerts[1]);
			quadTreeBack.SurfaceIndex = 4;
			quadTreeBack.Disabled = false;		
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeBack.MeshArray, compressFlags: 0);
	
			quadTreeDown = new QuadTree(this, QuadSide.Down, cubeVerts[0], cubeVerts[1], cubeVerts[4], cubeVerts[5]);
			quadTreeDown.SurfaceIndex = 5;
			quadTreeDown.Disabled = false;		
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeDown.MeshArray, compressFlags: 0);
	
			quadTreeUp = new QuadTree(this, QuadSide.Up, cubeVerts[6], cubeVerts[7], cubeVerts[2], cubeVerts[3]);
			quadTreeUp.SurfaceIndex = 0;
			quadTreeUp.Disabled = false;		
			arrayMesh.AddSurfaceFromArrays(Mesh.PrimitiveType.Triangles, quadTreeUp.MeshArray, compressFlags: 0);
		}
	
	}
}

