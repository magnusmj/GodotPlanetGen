shader_type spatial;
render_mode blend_mix,depth_draw_opaque,cull_back,diffuse_burley,specular_schlick_ggx;
uniform vec4 albedo : hint_color;
uniform sampler2D texture_albedo : hint_albedo;
uniform float specular;
uniform float metallic;
uniform float roughness : hint_range(0,1);
uniform float point_size : hint_range(0,128);
uniform sampler2D texture_normal : hint_normal;
uniform sampler2D detail_texture_normal : hint_normal;
uniform sampler2D texture_hight;
uniform float normal_scale : hint_range(-16,16);
uniform vec3 uv1_scale;
uniform vec3 uv1_offset;
uniform vec3 uv2_scale;
uniform vec3 uv2_offset;
uniform float height = 50.0;
varying vec2 UV3;
varying vec3 pos;

vec3 hash(vec3 p) {
    p = vec3(dot(p, vec3(127.1, 341.7, 174.7)),
             dot(p, vec3(469.5, 184.3, 246.1)),
             dot(p, vec3(113.515, 271.9, 124.65)));

    return -1.0 + 2.0 * fract(sin(p) * 43758.5453123);
}

float noise(vec3 p) {
  vec3 i = floor(p);
  vec3 f = fract(p);
  vec3 u = f * f * (3.0 - 2.0 * f);

  return mix(mix(mix(dot(hash(i + vec3(0.0, 0.0, 0.0)), f - vec3(0.0, 0.0, 0.0)),
                     dot(hash(i + vec3(1.0, 0.0, 0.0)), f - vec3(1.0, 0.0, 0.0)), u.x),
                 mix(dot(hash(i + vec3(0.0, 1.0, 0.0)), f - vec3(0.0, 1.0, 0.0)),
                     dot(hash(i + vec3(1.0, 1.0, 0.0)), f - vec3(1.0, 1.0, 0.0)), u.x), u.y),
             mix(mix(dot(hash(i + vec3(0.0, 0.0, 1.0)), f - vec3(0.0, 0.0, 1.0)),
                     dot(hash(i + vec3(1.0, 0.0, 1.0)), f - vec3(1.0, 0.0, 1.0)), u.x),
                 mix(dot(hash(i + vec3(0.0, 1.0, 1.0)), f - vec3(0.0, 1.0, 1.0)),
                     dot(hash(i + vec3(1.0, 1.0, 1.0)), f - vec3(1.0, 1.0, 1.0)), u.x), u.y), u.z );
}

float landDetail(vec3 loc){
	float n = noise(loc / 4200.0)*0.7;
	n += noise(loc / 400.0)*0.3;
	return n+0.5;
}

void vertex() {
	UV3 = UV*uv2_scale.xy*uv2_scale.xy+uv2_offset.xy;
	UV2 = UV*uv2_scale.xy+uv2_offset.xy;
	UV = UV*uv1_scale.xy+uv1_offset.xy;
	float th = texture(texture_hight,UV).x/1.5 + texture(texture_albedo,UV2).x/3.0;
	//VERTEX += height * NORMAL * th;
	pos = VERTEX;
	float n = landDetail(pos);
	
	float landmass = smoothstep(0.52,0.53, n);
	float landheight = smoothstep(0.52,1.0, n);
	VERTEX += height * NORMAL * th * landheight;

}


void fragment() {
	vec2 base_uv = UV;
	vec2 base_uv2 = UV2;
	vec2 base_uv3 = UV3;
	
	vec4 albedo_tex = texture(texture_albedo,base_uv2)*3.0;
	albedo_tex += texture(texture_albedo,base_uv3)*1.0;
	albedo_tex /= 2.0;
	
	float n = landDetail(pos);
	float landmass = smoothstep(0.52,0.53, n);
	float landheight = smoothstep(0.52,1.0, n);
	
	METALLIC = metallic;
	ROUGHNESS =  landmass*0.5+0.3;// albedo_tex.r+1.5;
	SPECULAR = 1.0-landmass;
	NORMALMAP = vec3(0.5,0.5,1);
	NORMALMAP += texture(texture_normal,base_uv).rgb*landheight;
	NORMALMAP += texture(detail_texture_normal,base_uv2).rgb*landheight;
	NORMALMAP += texture(detail_texture_normal,base_uv3).rgb*landheight;
	NORMALMAP /= 1.0 + 3.0*(landheight);
	NORMALMAP_DEPTH = normal_scale*1.5;
	
	float th = landheight*(texture(texture_hight,UV).x + texture(texture_albedo,UV2).x/3.0 - 0.0);
	float snow_height = smoothstep(0.545,0.550, th*(1.4 -2.5*(abs(NORMALMAP.r-0.5)+abs(NORMALMAP.g-0.5))));
	float beach_height = smoothstep(0.500,0.53, n) *  smoothstep(0.540,0.53, n);
	
	vec3 snow_color = vec3(1,1,1);
	vec3 ground_color = vec3(0.6,0.40,0.35);
	//vec3 beach_color = vec3(0.9,0.85,0.55)*0.6;
	vec3 beach_color = ground_color;
	vec3 water_color =  vec3(0.,0.03,0.05);
	ALBEDO = vec3(1,1,1) * albedo_tex.rgb*n *(1.0-snow_height - beach_height)*landmass*landheight + snow_color*snow_height + beach_color * beach_height + (1.0 - landheight- beach_height)*ground_color*landmass + (1.0-landmass)*water_color ;
	//ALBEDO.rgb = vec3(landmass);
	//ALBEDO.xyz = vec3(landmass*0.3);
	
}
