shader_type spatial;
render_mode blend_mix,depth_draw_alpha_prepass,cull_back;

const float pi = 3.141592;

varying float distotion;

void vertex() {
		
	
}

void fragment() {
	float AMOUNT = 1.0;
	float ASPECT = 1.0;
	float SIZE = 1.0;
	float DISTORTION = 1.0;
	float FOG = 0.0;
	vec4 normalmap = vec4(0.5,0.5, 1.0, 0.0);
	if (AMOUNT == 0.0) {
		ALPHA = 1.0;
		ALBEDO = vec3(0.0,0,0);
		vec2 ref_ofs = SCREEN_UV;
		ROUGHNESS = 0.0; 
		EMISSION += textureLod(SCREEN_TEXTURE,ref_ofs, 0).rgb;
	} else {
		float depth;
		depth = texture(DEPTH_TEXTURE, SCREEN_UV).r;
				
		depth = depth * 2.0 - 1.0;
		depth = PROJECTION_MATRIX[3][2] / (depth + PROJECTION_MATRIX[2][2]);
		depth = depth+VERTEX.z;
		
		vec2 uv = UV * vec2(ASPECT, 1.0)*SIZE;
		float depth_tex = textureLod(DEPTH_TEXTURE,SCREEN_UV,0.0).r;
		vec4 world_pos = INV_PROJECTION_MATRIX * vec4(SCREEN_UV*2.0-1.0,depth_tex*2.0-1.0,1.0);
		world_pos.xyz/=world_pos.w;
		ALPHA = clamp(-VERTEX.z/100.0, 0.0, 1.0);
		//ALPHA*=clamp(1.0-smoothstep(world_pos.z+1000.0,world_pos.z,VERTEX.z),0.0,1.0);
		
		
		float angle =  max(0, abs(dot(normalize(VERTEX), NORMAL)) - pi/80.0);
		ROUGHNESS = 1.0; 
		METALLIC = 0.0; 		
		depth *= angle/100.0;
		//depth /= 1000.0; 
		//depth = angle;
		ALBEDO = textureLod(SCREEN_TEXTURE,SCREEN_UV, FOG).rgb;
		//ALBEDO += vec3(angle/6.0,angle/6.0,angle);
		ALBEDO += vec3(depth/4.0,depth/4.0,depth);
		
	}
}